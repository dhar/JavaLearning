package com.zyt.demo;

// 使用注解表明该接口为函数式接口
@FunctionalInterface
public interface MyFunInterface {
    public abstract void say();
}
