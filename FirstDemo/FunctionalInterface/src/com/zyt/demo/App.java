package com.zyt.demo;

public class App {
    public static void main(String[] args) {
        sayHello(new MyFunInterfaceImpl());

        sayHello(new MyFunInterface() {
            @Override
            public void say() {
                System.out.println("匿名内部类Say");
            }
        });

        sayHello(() -> {
            System.out.println("Lambda 表达式Say");
        });
    }

    public static void sayHello(MyFunInterface obj) {
        obj.say();
    }
}
