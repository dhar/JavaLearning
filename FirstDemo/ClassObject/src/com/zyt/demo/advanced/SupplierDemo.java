package com.zyt.demo.advanced;

import java.util.Comparator;
import java.util.function.Supplier;

public class SupplierDemo {
    public static void main(String[] args) {
        String msgA = "Hello ";
        String msgB = "World ";

        System.out.println(getString(() -> msgA + msgB));
    }

    private static String getString(Supplier<String> fun) {
        return fun.get();
    }

    private static int getMax(Supplier<Integer> arr) {
        return arr.get();
    }

    private static Comparator<String> getComparator() {

        // 方式1，返回Comparator，并且重写compare方法
//        return new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.length() - o2.length();
//            }
//        };

        // 方式2：使用lambda表达式
//        return (String o1, String o2) -> {
//            return o1.length() - o2.length();
//        };
//    }

        // 方式3:使用优化后的lambda表达式
        return (o1, o2) -> o1.length() - o2.length();
    }
}
