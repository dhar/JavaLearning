package com.zyt.demo.advanced.methodRef;

public class MethodRefDemo {
    public static void printString(Printable printable) {
        printable.print("Hello World");
    }

    public static void main(String[] args) {

        // 使用匿名内部类
        printString(new Printable() {
            @Override
            public void print(String string) {
                System.out.println("string = " + string);
            }
        });

        // 使用lambda表达式，
        printString(s -> System.out.println(s));

        // 使用方法引用，使用System.out对象中的println处理参数值
        printString(System.out::println);

        // 自定义处理-使用Lambda
        printString( s -> {
            MyCustomMethondObject cumObj = new MyCustomMethondObject();
            cumObj.upperPrint(s);
        });

        // 自定义处理-使用方法引用
        MyCustomMethondObject cumObj = new MyCustomMethondObject();
        printString(cumObj::upperPrint);

        // 自定义处理-使用静态方法引用
        printString(MyCustomMethondObject::lowerPrint);
    }
}
