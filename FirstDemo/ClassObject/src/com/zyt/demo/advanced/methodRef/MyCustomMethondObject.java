package com.zyt.demo.advanced.methodRef;

public class MyCustomMethondObject {
    public void upperPrint(String string) {
        System.out.println("string.toUpperCase() = " + string.toUpperCase());
    }
    
    public static void lowerPrint(String string) {
        System.out.println("string.toLowerCase() = " + string.toLowerCase());
    }
}
