package com.zyt.demo.advanced.methodRef;

@FunctionalInterface
public interface Printable {
    public void print(String string);
}
