package com.zyt.demo.advanced;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Stream;

public class StreamDemo {
    public static void main(String[] args) {
        mapDemo();

        countDemo();

        limitDemo();

        skipDemo();

        concatDemo();
    }

    public static void streamDemo() {
        String[] names = {"张三", "李四", "张三丰"};

        // 使用容器类的stream()方法获取Stream
        List<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(names));
        Stream<String> listStream = list.stream();
        listStream.filter(name -> name.length() == 3)
                .filter(name -> name.startsWith("张"))
                .forEach(name -> System.out.println(name));

        Map<String, Object> map = new HashMap<String, Object>();
        Stream<String> mapKeyStream = map.keySet().stream();
        mapKeyStream.filter((String name) -> name.length() == 3);

        // 使用Stream.of方法获取Stream
        Stream<String> arrStream = Stream.of(names);

        // 使用Arrays.stream 方法获取Stream
        Arrays.stream(names);
    }

    public static void mapDemo() {
        String[] names = {"张三", "李四", "张三丰"};
        // Map
        Arrays.stream(names).map((name) -> name + "hh").forEach(
                (name) -> System.out.println(name)
        );
    }

    public static void countDemo() {
        String[] names = {"张三", "李四", "张三丰"};
        // Map
        long count = Arrays.stream(names).count();
        System.out.println("count = " + count);
    }

    public static void limitDemo() {
        System.out.println("\nlimit demo");
        String[] names = {"张三", "李四", "张三丰"};
        Stream stream = Arrays.stream(names).limit(2);
        stream.forEach(name -> System.out.println("name = " + name));


        System.out.println("\nlimit demo2");
        stream = Arrays.stream(names).limit(4);
        stream.forEach(name -> System.out.println("name = " + name));
    }

    public static void skipDemo() {
        System.out.println("\nSkip Demo");
        String[] names = {"张三", "李四", "张三丰"};
        Stream stream = Arrays.stream(names).skip(2);
        stream.forEach(name -> System.out.println("name = " + name));

        System.out.println("\nSkip Demo2");
        stream = Arrays.stream(names).skip(4);
        stream.forEach(name -> System.out.println("name = " + name));
    }

    public static void concatDemo() {
        System.out.println("\nconcat Demo");
        String[] names = {"张三", "李四", "张三丰"};
        Stream stream = Arrays.stream(names);

        String[] names2 = {"赵柳", "王五", "张三丰"};
        Stream stream2 = Arrays.stream(names);

        Stream.concat(stream, stream2).forEach(name -> System.out.println("name = " + name));
    }
}
