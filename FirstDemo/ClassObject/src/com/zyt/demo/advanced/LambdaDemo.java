package com.zyt.demo.advanced;

import sun.plugin2.message.Message;

public class LambdaDemo {
    public static void main(String[] args) {
        String msg1 = "Hello ";
        String msg2 = "World ";

        log(1, msg2 + msg2);
        log(2, msg2 + msg2);

        log2(1, ()-> msg2 + msg2);
        log2(2, ()-> msg2 + msg2);

        log2(1, new MessageBuilder() {
            @Override
            public String buildMsg() {
                return msg1 + msg2;
            }
        });

        log2(2, new MessageBuilder() {
            @Override
            public String buildMsg() {
                return msg1 + msg2;
            }
        });
    }

    public static void log(int level, String message) {
        if (level == 1) {
            System.out.println(message);
        }
    }

    public static void log2(int level, MessageBuilder builder) {
        if (level == 1) {
            System.out.println(builder.buildMsg());
        }
    }
}

interface MessageBuilder {
    String buildMsg();
}
