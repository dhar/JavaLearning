package com.zyt.demo.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class SocketDemo {
    public static void main(String[] args) throws IOException {
        Socket socket = new Socket("127.0.0.1", 6666);
        InputStream inputStream = socket.getInputStream();
        OutputStream outputStream = socket.getOutputStream();

        // 发送数据到服务端
        outputStream.write("你好我是客户端".getBytes());

        // 读取服务器数据
        byte[] datas = new byte[1024];
        int leng = inputStream.read(datas);
        String msg = new String(datas, 0, leng);
        System.out.println("客户端收到消息 msg = " + msg);
    }
}
