package com.zyt.demo.net;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketDemo {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(6666);
        Socket socket = serverSocket.accept();

        // 接收消息
        InputStream inputStream = socket.getInputStream();
        byte[] datas = new byte[1024];
        int len = inputStream.read(datas);
        String msg = new String(datas, 0, len);
        System.out.println("服务端收到消息 msg = " + msg);

        // 发送消息
        System.out.println("服务端开始发送消息");
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write("你好，我是服务端，收到了".getBytes());

        // 关闭
        inputStream.close();
        outputStream.close();
        socket.close();
    }
}
