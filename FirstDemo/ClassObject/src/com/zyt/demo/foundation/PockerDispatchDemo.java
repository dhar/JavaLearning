package com.zyt.demo.foundation;

import java.util.*;

class PockerCard {
    public enum Color {
        CAOH("♣"), FANP("♦"), HEIT("♠"), HONGT("♥");

        private String desc;

        Color(String desc) {
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public enum Number {

        NumA(1, "A"),
        Num2(2, "2"),
        Num3(2, "3"),
        Num4(4, "4"),
        Num5(5, "5"),
        Num6(6, "6"),
        Num7(7, "7"),
        Num8(8, "8"),
        Num9(9, "9"),
        Num10(10, "10"),
        NumJ(11, "J"),
        NumQ(12, "Q"),
        NumK(13, "K");

        private int index;
        private String desc;

        Number(int index, String desc) {
            this.index = index;
            this.desc = desc;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }
    }

    public enum CardType {
        Number, Joker;
    }

    private CardType type;
    private Number number;
    private Color color;

    public PockerCard(CardType type, Number number, Color color) {
        this.type = type;
        this.number = number;
        this.color = color;
    }

    @Override
    public String toString() {
        return "PockerCard{" +
                "type=" + type +
                ", number=" + number +
                ", color=" + color +
                '}';
    }

    public String friendlyShow() {
        if (this.type == CardType.Joker) {
            return "Jocker";
        }
        return color.desc + "" + number.desc;
    }
}

class PockerMachine {

    public class DispathcResult {
        private ArrayList<ArrayList<PockerCard>> playerCards;
        private ArrayList<PockerCard> leftCards;

        public ArrayList<ArrayList<PockerCard>> getPlayerCards() {
            return playerCards;
        }

        public void setPlayerCards(ArrayList<ArrayList<PockerCard>> playerCards) {
            this.playerCards = playerCards;
        }

        public ArrayList<PockerCard> getLeftCards() {
            return leftCards;
        }

        public void setLeftCards(ArrayList<PockerCard> leftCards) {
            this.leftCards = leftCards;
        }


    }

    private static HashMap<Integer, PockerCard> pockerMap;

    static {
        PockerCard.Color.values();
        PockerCard.Number.values();

        HashMap<Integer, PockerCard> tmpPockerMap = new HashMap<Integer, PockerCard>();
        int index = 0;
        // 添加数字牌
        for (PockerCard.Number number: PockerCard.Number.values()) {
            for (PockerCard.Color color : PockerCard.Color.values()) {
                PockerCard card = new PockerCard(PockerCard.CardType.Number, number, color);
                tmpPockerMap.put(index++, card);
            }
        }
        // 添加大王
        tmpPockerMap.put(index++, new PockerCard(PockerCard.CardType.Joker, null, null));
        tmpPockerMap.put(index++, new PockerCard(PockerCard.CardType.Joker, null, null));

        pockerMap = tmpPockerMap;
    }

    public String friendlyShow(int index) {
        PockerCard card = (PockerCard) pockerMap.get(index);
        return card.friendlyShow();
    }

    /**
     * 分牌
     * @param playerCount 玩家数量
     * @param leftCount 剩余牌数
     * @return
     */
    public DispathcResult dispatch(int playerCount, int leftCount) {

        if (leftCount > pockerMap.size()) {
            return null;
        }

        // 洗牌
        List cardIndexs = Arrays.asList(pockerMap.keySet().toArray());
        Collections.shuffle(cardIndexs);

        // 每一份牌张数
        int itemCount = (pockerMap.size() - leftCount) / playerCount;

        ArrayList playerCards = new ArrayList();

        for (int i = 0; i < playerCount; i++) {
            ArrayList onePlayerCards = new ArrayList();
            playerCards.add(onePlayerCards);
            List onePlayerCardIndexs = null;
            if (i == playerCount - 1) {
                onePlayerCardIndexs = cardIndexs.subList(i * itemCount, cardIndexs.size() - leftCount);
            } else {
                onePlayerCardIndexs = cardIndexs.subList(i * itemCount, (i + 1) * itemCount);
            }

            for (int j = 0; j < onePlayerCardIndexs.size(); j++) {
                Object idx = onePlayerCardIndexs.get(j);
                PockerCard card = pockerMap.get(idx);
                onePlayerCards.add(card);
                System.out.println();
            }
        }

        ArrayList leftCards = new ArrayList();
        for (int i = cardIndexs.size() - leftCount; i < cardIndexs.size(); i++) {
             PockerCard card = pockerMap.get(cardIndexs.get(i));
             leftCards.add(card);
        }

        DispathcResult result = new DispathcResult();
        result.leftCards = leftCards;
        result.playerCards = playerCards;

        return result;
    };
}

public class PockerDispatchDemo {
    public static void main(String[] args) {
        PockerMachine machine = new PockerMachine();
        PockerMachine.DispathcResult result = machine.dispatch(3, 3);
        int index = 0;
        for (ArrayList<PockerCard> onePlayerCards: result.getPlayerCards()) {
            System.out.println("玩家"+index++ + ":");
            for (PockerCard card: onePlayerCards) {
                System.out.print(card.friendlyShow() + " ");
            }
            System.out.println("");
        }

        System.out.println("底牌");
        for (PockerCard card: result.getLeftCards()) {
            System.out.print(card.friendlyShow() + " ");
        }

    }
}
