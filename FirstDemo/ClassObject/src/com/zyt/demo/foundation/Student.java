package com.zyt.demo.foundation;

import java.util.ArrayList;
import java.util.Scanner;

public class Student {
    // 成员变量会有默认值存在
    private String name;
    private  int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Student() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static void main(String[] args) {
        Student student1 = new Student();
        System.out.println("name = " + student1.name + " age = " + student1.age);

        Student student2 = new Student("XM", 12);
        System.out.println("name = " + student2.name + " age = " + student2.age);

        // 局部变量没有初始化的默认值，定义了变量之后不能直接使用，否则会报错
        String name;
        int age;
//        System.out.println("name = " + name + " age = " + age);
        name = "XM";
        age = 12;
        System.out.println("name = " + name + " age = " + age);

        ArrayList list = new ArrayList();
        list.add("A");
        list.add("B");
        System.out.println("size = " + list.size());

        Scanner scanner = new Scanner(System.in);
        System.out.println("输入一个整数");
        int a = scanner.nextInt();
        System.out.println("再输入一个整数");
        int b = scanner.nextInt();
        System.out.println("两者的和为：" + (a + b));

        String string = "Hello";
    }
}
