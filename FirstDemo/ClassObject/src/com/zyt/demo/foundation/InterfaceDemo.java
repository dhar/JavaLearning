package com.zyt.demo.foundation;

public class InterfaceDemo {
    public static void main(String[] args) {
        Duck duck = new MallarDuck();
        duck.display();;

        duck = new RubberDuck();
        duck.display();
    }
}

interface IFlyAble {
    public void fly();
}

interface IQuackable {
    public void quick();
}

class Duck {
    public void swim() {
        System.out.println("swim");
    }

    public void display() {
        System.out.println("Duck.display");
    }
}

class MallarDuck extends Duck implements IFlyAble {

    @Override
    public void fly() {

    }

    @Override
    public void display() {
        super.display();
        System.out.println("MallarDuck.display");
    }
}

class RubberDuck extends Duck implements IQuackable {

    @Override
    public void quick() {

    }

    @Override
    public void display() {
        super.display();
        System.out.println("RubberDuck.display");
    }
}


// ==============

interface Itest {

    // 常量定义
    public static final int ItestID = 123123;

    //    private void priFun();

    /**
     * 公有方法，子类需要重写
     */
    public void pubFun();

    /**
     * 抽象方法，子类需要重写
     */
    public abstract void absFun();

    /**
     * 默认方法default，需要有方法体，子类可以不重写，子类重写default需要去掉default修饰符
     */
    public default void defFun(){
        System.out.println("Itest.defFun");
    }

    /**
     * 静态方法static，需要有方法体，子类不能重写
     */
    public static void staFun() {
        System.out.println("Itest.staFun");
    }
}

class TestImpl implements Itest {

    @Override
    public void pubFun() {
        System.out.println("TestImpl.pubFun");
    }

    @Override
    public void absFun() {
        System.out.println("TestImpl.absFun");
    }

//    @Override
//    public void defFun() {
//        System.out.println("TestImpl.defFun");
//    }

    public static void main(String[] args) {
        TestImpl impl = new TestImpl();
        impl.pubFun();
        impl.absFun();

        Itest.staFun();
        impl.defFun();;

        // 匿名内部类的使用
        Itest test = new Itest() {
            @Override
            public void pubFun() {
                System.out.println("new Itest().pubFun");
            }

            @Override
            public void absFun() {
                System.out.println("new Itest().absFun");
            }
        };
        test.pubFun();
        test.absFun();
        impl.defFun();
    }
}

