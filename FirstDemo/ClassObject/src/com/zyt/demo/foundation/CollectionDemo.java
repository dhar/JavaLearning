package com.zyt.demo.foundation;

import java.util.ArrayList;
import java.util.List;

public class CollectionDemo {
    public static void main(String[] args) {
        arraylistDemo();
    }

    public static void arraylistDemo() {
        // 创建列表
        ArrayList list = new ArrayList();

        // 空判断&大小
        System.out.println("is empty " + list.isEmpty());//true
        System.out.println("size " + list.size());//0

        // 添加元素
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");

        // 空判断&大小
        System.out.println("is empty " + list.isEmpty());//false
        System.out.println("size " + list.size());//4

        // 删除
        list.remove("B");
        list.remove(0);
        System.out.println(list);//[C, D]

        // 修改
        list.add(0, "A");
        list.set(0, "A-a");
        System.out.println(list);//[A-a, C, D]

        // 查询
        // 位置查询
        System.out.println(list.get(0)); // A-a
        // 查询元素所在位置
        System.out.println(list.indexOf("C"));//1
        System.out.println(list.indexOf("Z"));//-1
        // 是否包含
        System.out.println(list.contains("Z"));//false
        System.out.println(list.contains("D"));//true
    }

    public static void collectionDemo(){

    }
}
