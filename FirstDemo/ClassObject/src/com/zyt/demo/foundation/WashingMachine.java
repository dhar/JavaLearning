package com.zyt.demo.foundation;

public class WashingMachine {
    private String brand;/*品牌*/
    private int capacity;/*容量*/

    public WashingMachine(String brand, int capacity) {
        this.brand = brand;
        this.capacity = capacity;
    }

    /**
     * 开始
     */
    public void start() {
        System.out.println("开始");
    }

    /**
     * 结束
     */
    public void stop() {
        System.out.println("结束");
    }

    public static void main(String[] args) {
        WashingMachine machine = new WashingMachine("Hair", 40);
        machine.capacity = 50;
        machine.start();
        machine.stop();
    }
}
