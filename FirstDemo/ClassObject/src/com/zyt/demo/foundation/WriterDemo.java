package com.zyt.demo.foundation;

import java.io.*;

public class WriterDemo {
    public static void main(String[] args) throws IOException {
        {
            File file = new File("utf8_out.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            // 文件字符流，默认是UTF-8编码
            FileWriter fileWriter = new FileWriter(file, true);
            fileWriter.write("你好");
            fileWriter.flush();
            fileWriter.close();
        }

        {
            File file = new File("gbk_out.txt");
            if (!file.exists()) {
                file.createNewFile();
            }

            // 字符转换流，支持设置编码
            OutputStreamWriter osw = new OutputStreamWriter(new FileOutputStream(file, true), "GBK");
            osw.write("你好");
            osw.flush();
            osw.close();


        }


    }
}
