package com.zyt.demo.foundation;

public class ThreadDemo {
    public static void main(String[] args) {
        System.out.println(Thread.currentThread() + " Running");

//        MyThread thread = new MyThread("my thread");
//        thread.start();

        // 使用Runnable
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Runnable Thread = " + Thread.currentThread());
            }
        }).start();;

        // 使用Lambda
        new Thread(() -> {
            System.out.println("Lambda Thread = " + Thread.currentThread());
        }).start();;
    }
}

class MyThread extends  Thread {

    public MyThread(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread() + " Running");
    }
}
