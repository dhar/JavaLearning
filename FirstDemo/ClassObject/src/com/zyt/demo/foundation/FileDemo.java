package com.zyt.demo.foundation;

import java.io.File;

public class FileDemo {
    public static void main(String[] args) {
        File file = new File("/tmp");
        System.out.println("absPath = " + file.getAbsolutePath() + " exists = " + file.exists());

        File file1 = new File("/tmp", "llvm-bcanalyzer");
        System.out.println("file1.Path = " + file1.getPath());
        System.out.println("file1.AbsPath = " + file1.getAbsolutePath());

        File file2 = new File("Resource/Stream/Test.txt");
        System.out.println("file2.Path = " + file2.getPath());
        System.out.println("file2.AbsPath = " + file2.getAbsolutePath());
    }
}
