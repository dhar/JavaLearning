package com.zyt.demo.foundation;

class GenericTester <T> {
    public void show(T t) {
        System.out.println("t = " + t);
        System.out.println("t.class = " + t.getClass());
    }

    public <E> void show2(E e) {
        System.out.println("e = " + e);
        System.out.println("e.class = " + e.getClass());
    }
}

class GenericFunTester {
    public <E> void show2(E e) {
        System.out.println("e = " + e);
        System.out.println("e.class = " + e.getClass());
    }
}

public class GenericDemo {
    public static void main(String[] args) {
        GenericTester<String> tester = new GenericTester<String>();
        tester.show("Hello");
        tester.show2(1.22);
        tester.show2(122);
    }
}
