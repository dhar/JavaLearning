package com.zyt.demo.foundation;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateDemo {
    public static void main(String[] args) throws ParseException {
        Date date = new Date();
        System.out.println("date = " + date);
        date = new Date(0L);
        System.out.println("date = " + date);

        long timeStamp = new Date().getTime();
        System.out.println("timeStamp = " + timeStamp); // 1582637935075

// Date & DateFormat Demo
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String formatedDateString = format.format(new Date());
        System.out.println("formatedDateString = " + formatedDateString); // 2020-02-25 21:38:55

        Date parsedDate = format.parse("2010-12-12 12:12:12");
        System.out.println("parsedDate = " + parsedDate);//Sun Dec 27 12:12:12 CST 2009

// Calendar
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        System.out.println("year = " + year);
        int month = calendar.get(Calendar.MONTH);
        System.out.println("month = " + month);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println("day = " + day);

        calendar.set(Calendar.YEAR, 2022);
        date = calendar.getTime();
        System.out.println("date = " + date);//Sat Feb 26 10:10:10 CST 2022

        calendar.add(Calendar.DAY_OF_MONTH, 1);
        date = calendar.getTime();
        System.out.println("date = " + date);//Sun Feb 27 10:11:12 CST 2022

        calendar.add(Calendar.DAY_OF_MONTH, -3);
        date = calendar.getTime();
        System.out.println("date = " + date);//Thu Feb 24 10:11:12 CST 2022

        calendar.add(Calendar.DAY_OF_MONTH, 10);
        date = calendar.getTime();
        System.out.println("date = " + date);//Sun Mar 06 10:11:12 CST 2022
    }
}
