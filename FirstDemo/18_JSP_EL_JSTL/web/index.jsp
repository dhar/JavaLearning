<%--
  Created by IntelliJ IDEA.
  User: aron
  Date: 2021/2/6
  Time: 11:33
  To change this template use File | Settings | File Templates.
--%>

<%--
1. 指令
  - 作用：用于配置JSP页面，导入资源文件
  - 格式：<%@ %>
  - 分类：
    - page
      - contentType: 等同于 setContentType
        - 设置当前页面的字符集编码
        - 设置响应体的字符集编码
      - lauguage 设置语言
      - buffer 缓冲区
      - import 导入Java包
      - errorPage 错误的时候跳转到的错误页面
      - isErrorPage 是否是错误页面。
        - true 可以使用内置对象 exception
        - false 不能使用内置对象 exception
    - include
    - taglib
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java"
         import="java.util.Date" errorPage="error.jsp"
         isErrorPage="false" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.HashMap" %>
<%@include file="top.jsp" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>

<%--
这个是JSP注释
--%>

<%--
内置对象
- pageContext       PageContext             当前页面共享对象，可以获取其他的内置对象
- request           HttpServletRequest      一次请求访问的多个资源（转发）
- session           HttpSeesion             一次会话的多个请求间
- application       ServletContext          多个用户间共享
- response          HttpServletResponse     相应对象
- page              Object                  当前页面
- out               JSPWriter               输出对象，输出数据到页面上
- config            ServletConfig           Servlet配置对象
- exception         Throwable               异常对象


EL 表达式
- 概念：Expression Language 表达式语言
- 作用：替换JSP页面中Java代码的编写
- 语法：${表达式}
--%>


<%!
    ArrayList list = new ArrayList();
%>

<%
    /* pageContext */
    pageContext.setAttribute("msg", "hello 啊");

    list.add("aaaa");
    pageContext.setAttribute("list", list);

    Map map = new HashMap();
    map.put("name", "Name From Map");
    pageContext.setAttribute("map", map);
%>

<%
    /* pageContext */
    String msg = (String) pageContext.getAttribute("msg");
    System.out.println("msg = " + msg);
%>

<h2>EL Demo</h2>

<h3>EL运算</h3>

\${1>6}
${1>6}
<br/>

\${1/6}
${1/6}
<br/>

\${1%6}
${1%6}
<br/>

\${empty list}
${empty list}
<br/>

<h3>获取值</h3>
<%--
- el表达式只能从域对象中获取值
- 语法：${域名称.键值}
--%>
<%
    pageContext.setAttribute("name", "Name From PageContext");
    request.setAttribute("name", "Name From Request");
    session.setAttribute("name", "Name From Session");
    application.setAttribute("name", "Name From Application");
%>

${name}<br>
${pageScope.name}<br>
${requestScope.name}<br>
${sessionScope.name}<br>
${applicationScope.name}<br>

${list}<br>
${list[0]}<br>
${map}<br>
${map.name}<br>

<h3>EL 隐式对象</h3>
contextPath=${pageContext.request.contextPath}<br>

</body>
</html>
