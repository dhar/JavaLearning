package com.zyt.demo.reflect;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Demo {
    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class pClass = Person.class;

        Person person = new Person();

        Method pEatMethod = pClass.getMethod("eat");
        pEatMethod.invoke(person);
    }
}
