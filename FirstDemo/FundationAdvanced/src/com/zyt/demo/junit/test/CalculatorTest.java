package com.zyt.demo.junit.test;

import com.zyt.demo.junit.Calculator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculatorTest {

    // 在测试方法执行之前执行
    @Before
    public void init() {
        System.out.println("====init");
    }


    // 在测试方法执行之后执行
    @After
    public void close() {
        System.out.println("====close");
    }


    @Test
    public void testAdd() {
        System.out.println("testAdd");

        Calculator calculator = new Calculator();
        int result = calculator.add(1, 5);
        // 判定结果，绿色成功，红色失败
        // 使用断言操作处理结果
        Assert.assertEquals(result, 6);
    }

    @Test
    public void testSub() {
        System.out.println("testSub");

        Calculator calculator = new Calculator();
        int result = calculator.sub(1, 5);
        // 判定结果，绿色成功，红色失败
        // 使用断言操作处理结果
        Assert.assertEquals(result, -4);
    }
}
