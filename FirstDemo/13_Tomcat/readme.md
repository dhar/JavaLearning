Idea 中使用Tomcat部署项目，可以看到控制台输出如下日志
```shell script
24-Jan-2021 14:09:17.729 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_BASE:         /Users/aron/Library/Caches/IntelliJIdea2019.3/tomcat/Tomcat_8_0_29_FirstDemo_5
24-Jan-2021 14:09:17.729 INFO [main] org.apache.catalina.startup.VersionLoggerListener.log CATALINA_HOME:         /Users/aron/softwares/apache-tomcat-8.0.29
```

`/Users/aron/Library/Caches/IntelliJIdea2019.3/tomcat/Tomcat_8_0_29_FirstDemo_5` 是项目部署配置路径，进入该路径下，找到`16_Response_war_exploded.xml`配置文件  

![](https://gitee.com/dhar/BlogPictures/raw/master/20210124141529.png)

该配置文件的内容如下，其中docBase的路径就是真实的项目部署的路径    

```shell script
<Context path="/16_Response_war_exploded" docBase="/Users/aron/GitRepo/JavaLearning/FirstDemo/out/artifacts/16_Response_war_exploded" />
```

具体部署内容如下所示 

![](https://gitee.com/dhar/BlogPictures/raw/master/20210124142212.png)