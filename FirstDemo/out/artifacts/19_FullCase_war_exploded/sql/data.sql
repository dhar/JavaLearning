create database 19_FullCase;
use 19_FullCase;
create table if not exists  user_t(
    id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(32),
    age INTEGER,
    gender VARCHAR(4),
    password VARCHAR(32),
    qq VARCHAR(32),
    address VARCHAR(32)
);


INSERT INTO user_t (name, age, gender, password, qq, address)
 VALUES ("张三", 32, "男", "123456", "zs@qq.com", "福建");
INSERT INTO user_t (name, age, gender, password, qq, address)
VALUES ("李四", 22, "女", "123456", "ls@qq.com", "北京");

SELECT * FROM user_t;
SELECT * FROM user_t limit 0,5;
SELECT * FROM user_t limit 5,5;
SELECT COUNT(*) FROM user_t;