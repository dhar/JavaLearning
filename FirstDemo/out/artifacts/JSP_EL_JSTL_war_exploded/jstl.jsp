<%@ page import="java.util.List" %>
<%@ page import="java.util.ArrayList" %><%--
  Created by IntelliJ IDEA.
  User: aron
  Date: 2021/2/6
  Time: 15:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>JSTL Demo</title>
</head>
<body>

<%--
- 概念：Java Server Pages Tag Library JSP 标准标签库
- 作用：简化和替换JSP中的Java代码
- 使用
    - 导入JSTL jar包
    - taglib引入标签库
    - 使用

- 标签介绍
    - c:if 条件表达式 如何条件为真，执行标签体
    - c:choose SwitchCase
    - c:forEach 循环遍历
        - begin 开始
        - end 结束
        - var 临时变量保存当前遍历值
        - step 步长
--%>

<h2>JSTL Demo </h2>

<%
    List list = new ArrayList();
    list.add("aaaa");
    list.add("bbb");
    list.add("ccc");
    request.setAttribute("list", list);

    request.setAttribute("num", 3);
%>


<h3>c:if</h3>

<c:if test="true">
    <div>我是真，haha</div>
</c:if>


<c:if test="${not empty list}">
    遍历集合
</c:if>


<h3>c:choose</h3>

<c:choose>
    <c:when test="${num == 1}">星期一</c:when>
    <c:when test="${num == 2}">星期二</c:when>
    <c:when test="${num == 3}">星期三</c:when>
    <c:when test="${num == 4}">星期四</c:when>
    <c:otherwise>数字有误</c:otherwise>
</c:choose>

<h3>c:forEach</h3>
<c:forEach begin="0" end="5" var="i" step="1" varStatus="s">
    ${i}-${s.index}-${s.count}<br>
</c:forEach>
<br>

<c:forEach begin="0" end="5" var="i" step="2" varStatus="s">
    ${i}-${s.index}-${s.count}<br>
</c:forEach>
<br>

<c:forEach items="${list}" var="item" varStatus="s">
${item}-${s.index}-${s.count}<br>
</c:forEach>
<br>

</body>
</html>
