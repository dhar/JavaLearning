package com.zyt.login.dao;

import com.zyt.login.domain.User;
import com.zyt.login.util.JDBCUtil;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

public class UserDao {
    private JdbcTemplate template = new JdbcTemplate(JDBCUtil.getDatasource());

    public User login(User loginUser) {
        String sql = "select * from user_table where username = ? and password = ?";
        User user = null;
        try {
            user = template.queryForObject(sql,
                    new BeanPropertyRowMapper<User>(User.class),
                    loginUser.getUsername(),
                    loginUser.getPassword());
        } catch (Exception e) {
            System.out.println("获取对象出错");
            e.printStackTrace();
        }
        return user;
    }
}
