package com.zyt.login.util;

import com.alibaba.druid.pool.DruidDataSourceFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 * JDBC 工具类，使用Durid连接池
 */
public class JDBCUtil {
    private static DataSource ds;

    static {
        try {
            // Prop 加载
            Properties properties = new Properties();
            properties.load(JDBCUtil.class.getClassLoader().getResourceAsStream("ddruid.properties"));
            ds = DruidDataSourceFactory.createDataSource(properties);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static DataSource getDatasource() {
         return ds;
    }

    public static Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
