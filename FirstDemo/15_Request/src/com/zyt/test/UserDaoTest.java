package com.zyt.test;

import com.zyt.login.dao.UserDao;
import com.zyt.login.domain.User;
import org.junit.Test;

public class UserDaoTest {
    @Test
    public void testGetUser() {
        User loginUser = new User();
        loginUser.setUsername("superbaby1");
        loginUser.setPassword("123");

        UserDao dao = new UserDao();
        User user = dao.login(loginUser);

        System.out.println("user = " + user);
    }
}
