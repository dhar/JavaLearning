package com.zyt.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/demo2")
public class RequestDispatcherDemo extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 请求转发给demo1处理
        // 设置域对象，可以共享数据
        req.setAttribute("msg", "hello");
        req.getRequestDispatcher("/demo1").forward(req, resp);

        // 转发的特点
        // 地址栏不变
        // 只发生一次请求
        // 只能访问当前服务器的资源
        // 可以使用域对象共享数据
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }
}
