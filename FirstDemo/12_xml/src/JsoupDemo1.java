import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class JsoupDemo1 {
    public JsoupDemo1() {
    }

    public static void main(String[] args) throws IOException {
        URL xmlURL = JsoupDemo1.class.getClassLoader().getResource("students.xml");
        String xmlPath = xmlURL.getPath();
        Document document = Jsoup.parse(new File(xmlPath), "UTF-8");

        ArrayList studentElements = document.getElementsByTag("student");
        int count = studentElements.size();
        System.out.println("count = " + count);

        System.out.println();
        System.out.println("getElementById");
        Element element = document.getElementById("0001");
        System.out.println("element = " + element);

        System.out.println("");
        System.out.println("getElementsByAttribute");
        studentElements = document.getElementsByAttribute("id");
        System.out.println("studentElements.size = " + studentElements.size());

        System.out.println();
        System.out.println("\ngetElementsByAttributeValue");
        studentElements = document.getElementsByAttributeValue("id", "0002");
        System.out.println("studentElements = \n" + studentElements);

        System.out.println();
        System.out.println("select  #0001");
        studentElements = document.select("#0001");
        System.out.println("studentElements = " + studentElements);

        System.out.println();
        System.out.println("select  name");
        studentElements = document.select("name");
        System.out.println("studentElements = " + studentElements);

        System.out.println("");
        System.out.println("select  student[id=0002]");
        studentElements = document.select("student[id=0002]");
        System.out.println("studentElements = " + studentElements);

    }
}
