package com.zyt.demo;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class HttpServer {
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8888);
        while (true) {
            Socket clientSocket = serverSocket.accept();
            handleConnection(clientSocket);
        }
    }

    private static void handleConnection(Socket clientSocket) throws IOException {
        new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            doHandleConnection(clientSocket);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).start();
    }

    private static void doHandleConnection(Socket clientSocket) throws IOException {
        HttpRequestHeader header = getRequestHeader(clientSocket);
        if (header != null) {

            OutputStream os = clientSocket.getOutputStream();

            // 写入响应头
            os.write("HTTP/1.1 200 OK\r\n".getBytes());
            os.write("Content-Type:text/html\r\n".getBytes());
            os.write("\r\n".getBytes());

            // 写入文件内容
            String path = "." + header.path;
            File file = new File(path);
            System.out.println("file.exists() = " + file.exists());
            FileInputStream fis = new FileInputStream(file);

            byte[] bytes = new byte[1024];
            int len = 0;
            while ((len = fis.read(bytes)) > 0) {
                os.write(bytes);
            }

            os.flush();
            os.close();

            fis.close();
        }
    }

    private static HttpRequestHeader getRequestHeader(Socket clientSocket) throws IOException {
        InputStream is = clientSocket.getInputStream();

        byte[] bytes = new byte[1024];
        int len = 0;
        len = is.read(bytes);
        if (len > 0) {
            String line = new String(bytes, 0, len);
            System.out.println("line = " + line);

            String[] headerCoponents = line.split("\r\n");
            if (headerCoponents.length > 0) {
                HttpRequestHeader header = new HttpRequestHeader();

                // 处理请求头第一行
                String pathLine = headerCoponents[0];
                String[] pathComponents = pathLine.split(" ");
                if (pathComponents.length >= 3) {
                    String method = pathComponents[0];
                    String path = pathComponents[1];
                    String httpVersion = pathComponents[2];
                    header.method = method;
                    header.path = path;
                    header.httpVersion = httpVersion;
                }

                return header;
            }
        }
        return null;
    }
}


// 浏览器发起的HTTP请求的请求头
// GET /web/index.html HTTP/1.1
// Host: 127.0.0.1:8888
// Connection: keep-alive
// Upgrade-Insecure-Requests: 1
// User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36
// Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
//Sec-Fetch-Site: none
//Sec-Fetch-Mode: navigate
//Sec-Fetch-User: ?1
//Sec-Fetch-Dest: document
//Accept-Encoding: gzip, deflate, br
//Accept-Language: zh-CN,zh;q=0.9
