package com.zyt.fullcase.test;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyTest {
    public static void main(String[] args) {
        LenovoImpl lenovo = new LenovoImpl();

        IComputer lenovoProxy = (IComputer) Proxy.newProxyInstance(lenovo.getClass().getClassLoader(), lenovo.getClass().getInterfaces(), new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("proxy invoke");
//                System.out.println("proxy = " + proxy);
                System.out.println("method = " + method);
                System.out.println("args = " + args);

                // 调用真实对象的方法
                Object obj = method.invoke(lenovo, args);
                return obj;
            }
        });

        lenovoProxy.show();;
    }
}
