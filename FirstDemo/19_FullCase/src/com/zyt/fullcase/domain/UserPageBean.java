package com.zyt.fullcase.domain;

import java.util.List;

public class UserPageBean {
    private List<User> users;
    private Integer contentNumOfPage;
    private Integer currentPage;
    private Integer totalPage;
    private Integer totalCount;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getContentNumOfPage() {
        return contentNumOfPage;
    }

    public void setContentNumOfPage(Integer contentNumOfPage) {
        this.contentNumOfPage = contentNumOfPage;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Integer getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }
}
