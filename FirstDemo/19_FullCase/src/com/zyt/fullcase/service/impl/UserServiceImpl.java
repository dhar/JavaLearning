package com.zyt.fullcase.service.impl;

import com.zyt.fullcase.dao.UserDao;
import com.zyt.fullcase.dao.impl.UserDaoImpl;
import com.zyt.fullcase.domain.User;
import com.zyt.fullcase.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {
    private UserDao dao;

    public UserServiceImpl() {
        this.dao = new UserDaoImpl();
    }

    @Override
    public User getUser(String name, String password) {
        return this.dao.getUser(name, password);
    }

    @Override
    public List<User> getAllUsers() {
        return this.dao.getAllUsers();
    }

    @Override
    public void deleteUser(String userId) {
        this.dao.deleteUser(userId);
    }

    @Override
    public User getUser(String userId) {
        return this.dao.getUser(userId);
    }

    @Override
    public void updateUser(User user) {
        this.dao.updateUser(user);
    }

    @Override
    public Integer totalCount() {
        return this.dao.totalCount();
    }

    @Override
    public List<User> getUsers(Integer page, Integer limit) {
        return this.dao.getUsers(page, limit);
    }
}
