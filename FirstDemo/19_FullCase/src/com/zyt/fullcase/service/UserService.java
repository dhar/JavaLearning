package com.zyt.fullcase.service;

import com.zyt.fullcase.domain.User;

import java.util.List;

public interface UserService {
    User getUser(String name, String password);
    List<User> getAllUsers();
    void deleteUser(String userId);
    User getUser(String userId);
    void updateUser(User user);
    Integer totalCount();
    List<User> getUsers(Integer page, Integer limit);
}
