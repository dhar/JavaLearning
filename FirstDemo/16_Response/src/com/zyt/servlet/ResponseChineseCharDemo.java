package com.zyt.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/responseChineseCharDemo")
public class ResponseChineseCharDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 设置输出流编码
        resp.setCharacterEncoding("UTF-8");
        // 设置ContentType
        resp.setContentType("text/html;charset=utf-8");
        // 输出中文
        resp.getWriter().write("你好");
    }
}
