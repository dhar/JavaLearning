package com.zyt.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/dispatchDemo")
public class DispatchDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 转发到responseDemo2，使用绝对路径，不需要加虚拟目录
        resp.getWriter().write("转发到 responseDemo2");
        req.getRequestDispatcher("/responseDemo2").forward(req, resp);
    }
}
