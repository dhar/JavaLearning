package com.zyt.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/responseDemo1")
public class ResponseDemo1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("=====Demo1");
        resp.getWriter().write("=====Demo1");
        // 重定向到 /16_Response_war_exploded/responseDemo2
        // 方法一：设置status和header
//        resp.setStatus(302);
//        resp.setHeader("location", "/16_Response_war_exploded/responseDemo2");

        // 方法二：使用JavaEE框架中的sendRedirect方法
        req.setAttribute("name", "zyt");
        String cxtPath = req.getContextPath();///16_Response_war_exploded
        resp.sendRedirect(cxtPath + "/responseDemo2");
        // 访问外部资源
//        resp.sendRedirect("http://www.baidu.com");

        // 特点
        // 地址栏发生变化，发生两次请求
        // 可以访问外部的资源
        // 不能使用域对象共享数据（Request.getAttribute）

    }
}
