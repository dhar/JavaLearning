package com.zyt.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/redirectDemo")
public class RedirectDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 重定向是从客户端发出的，使用绝对路径，需求添加虚拟目录
        // 虚拟目录有可能是会变化的，使用动态获取的方式
        String ctxPath = req.getContextPath();
        System.out.println("ctxPath = " + ctxPath);
        resp.sendRedirect(ctxPath + "/responseDemo2");
    }
}
