package com.zyt.servletContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@WebServlet("/fileReadDemo")
public class FileReadDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // web根目录下的文件
        String path = this.getServletContext().getRealPath("b.txt");
        readFile(path);

        //获取WEB-INF目录下的文件
        path = this.getServletContext().getRealPath("WEB-INF/c.txt");
        readFile(path);

        // src 目录下的文件
        path = this.getServletContext().getRealPath("WEB-INF/classes/a.txt");
        readFile(path);
    }

    private void readFile(String path) throws IOException {
        FileReader reader = new FileReader(path);
        BufferedReader reader1 = new BufferedReader(reader);
        String line = reader1.readLine();
        System.out.println("line = " + line);
    }
}
