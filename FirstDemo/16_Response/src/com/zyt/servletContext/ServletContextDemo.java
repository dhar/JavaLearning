package com.zyt.servletContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/servletContextDemo")
public class ServletContextDemo extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context1 = req.getServletContext();
        ServletContext context2 = this.getServletContext();
        if (context1 == context2) {
            System.out.println("相等");
        }

        // 获取MineType
        String mineType = context1.getMimeType("a.jpg");
        System.out.println("mineType = " + mineType);

        // 数据共享
        // 可以共享用户所有的请求数据
        // 生命周期和服务器的生命周期一致，谨慎使用，防止内存泄漏问题
        context1.setAttribute("name", "zyt");
    }
}
