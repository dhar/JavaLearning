package com.zyt.demo.cookie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CookieServlet")
public class CookieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("request = " + request);
/*
         - 基础
            - 会通过Response的header发送给客户端
            - 可以发送多个Cookie 方法：addCookie()
         - 生命周期
            - 默认 Cookie 保存在浏览器的内存中，如果浏览器关闭(需要完全退出进程)，那么Cookie失效
            - set-cookie:name=zyt
            - 设置Cookie的持久化时间可以把Cookie内容写入到硬盘的文件中
         - 中文数据支持，
            - Tomcat8之前不能直接存储中文数据，Tomcat8之后可以正常存储中文
            - Tomcat8 之前需要把数据进行URL编码进行保存
         - Cookie范围
            - 当前服务器
                - 默认是在当前虚拟目录下共享
                - 可以设置为/在当前WEB服务器上的所有项目共享
            - 不同服务端之间的Cookie共享
                - 设置Cookie一级域名相同，多台服务器可以共享Cookie
                - 方法：setDomain() eg.setDomain(".baidu.com") 那么 tieba.baidu.com news.baidu.com 这两者可以共享Cookie

         - 特点和作用
            - 特点
                - 数据存储在客户端
                - 单个大小限制4KB，同一个域名下的Cookie数量限制20个
            - 作用
                - 存储骚年的不敏感数据
                - 在不登录的情况下，完成服务端对客户端的身份验证
 */
        Cookie cookie = new Cookie("name", "zyt");
        response.addCookie(cookie);

        // 设置Cookie的持久化时间可以把Cookie内容写入到硬盘的文件中
        cookie = new Cookie("age", "23");
        cookie.setMaxAge(100);// 时间单位为S
        response.addCookie(cookie);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
