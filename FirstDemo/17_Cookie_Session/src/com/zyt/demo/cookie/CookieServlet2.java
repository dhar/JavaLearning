package com.zyt.demo.cookie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/CookieServlet2")
public class CookieServlet2 extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 客户端会通过Request的Header把cookie发送给服务端
        // cookie: name=zyt;name2=value2
        Cookie[] cookies = request.getCookies();
        response.getWriter().write("<table border=1>\n");
        response.getWriter().write("<tr><th>Name</th><th>Value</th></tr>\n");
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                response.getWriter().write("<tr>");

                String name = cookie.getName();
                String value = cookie.getValue();
                System.out.println("name = " + name);
                System.out.println("value = " + value);

                response.getWriter().write("<td>" + name + "</td>");
                response.getWriter().write("<td>" + value + "</td>");

                response.getWriter().write("</tr>\n");

            }
        }
        response.getWriter().write("</table>");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
