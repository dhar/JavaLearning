<%--
  Created by IntelliJ IDEA.
  User: aron
  Date: 2021/1/24
  Time: 15:17
  To change this template use File | Settings | File Templates.
--%>
<%--
- 原理
  - JSP本质上是Servlet
- JSP 脚本：JSP定义Java代码的方式
  - <% 代码 %> ，在service方法中，service中可以定义什么该脚本中就能定义什么
  - <%! 代码 %>，在JSP转换后的Java类的成员变量
  - <%= 代码 %>，输出语句，可以吧变量输出到页面中
- JSP内置对象 在JSP页面中可以直接获取的对象，一共有九个内置对象
  - request
  - response
  - out 类似数据输出流，类似 response.getWriter()
    - 区别 response.getWriter() 输出的内容优先级会高于out，因为他们有独立的缓冲区
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<%!
  // 定义的是成员变量
  int i = 4;
  String title = "hello title";
%>

  <head>
    <title><%= title %></title>
  </head>
  <body>

  <%
    // 定义在service方法中
    int i = 5;
    System.out.println("hello world");
    // 使用out输出到页面
    out.write("i = " + i);
  %>

  <%
    System.out.println("i = " + i);
  %>
  </body>
</html>
