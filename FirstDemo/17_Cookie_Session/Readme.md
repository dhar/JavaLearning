Cookie Session

- 会话技术
    - Cookie
    - Session
    
## 会话技术
- 会话：一次会话包含多次请求和相应
- 功能：再一次会话的范围内的多次请求间，共享数据
- 方式
  - 客户端会话技术：Cookie
  - 服务端会话技术：Session
  
