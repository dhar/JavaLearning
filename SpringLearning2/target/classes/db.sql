create database eesy_spring;
use eesy_spring;

create table account (
    ID integer primary key auto_increment,
    uid integer,
    money integer
);
alter table account add name varchar(20);

insert into eesy_spring.account(uid, money, name) values (12, 1234, "小明");
insert into eesy_spring.account(uid, money, name) values (13, 123456, "小许");

-- Test
select * from eesy_spring.account;
delete from eesy_spring.account where ID=1;

update eesy_spring.account set name="小明" where ID=2;
