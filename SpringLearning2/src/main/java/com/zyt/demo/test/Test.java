package com.zyt.demo.test;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.zyt.demo.domain.Account;
import com.zyt.demo.service.IAccountService;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class Test {

    @org.junit.Test
    public void testRunner() {
        QueryRunner runner = new QueryRunner();
        Connection connection = null;
        String sql = "";
        try {
            runner.query(connection,sql,new BeanListHandler<Account>(Account.class));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @org.junit.Test
    public void testConnPool() {
        ComboPooledDataSource dataSource = new ComboPooledDataSource();
        try {
            dataSource.setDriverClass("com.mysql.jdbc.Driver");
        } catch (PropertyVetoException e) {
            e.printStackTrace();
        }
        dataSource.setJdbcUrl("jdbc:mysql://localhost:3306/eesy_spring");
        dataSource.setUser("root");
        dataSource.setPassword("1234");
        Connection connection = null;
        try {
            connection = dataSource.getConnection();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println("connection = " + connection);
    }

    @org.junit.Test
    public void testFindAll() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        IAccountService accountService = context.getBean("accService", IAccountService.class);
        List accounts = accountService.findAllAccount();
        System.out.println("accounts = " + accounts);
    }

    @org.junit.Test
    public void testFindAllWithAnno() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean_anno.xml");
        IAccountService accountService = context.getBean("accoutService", IAccountService.class);
        List accounts = accountService.findAllAccount();
        System.out.println("accounts = " + accounts);
    }
}
