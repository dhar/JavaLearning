package com.zyt.demo.service.impl;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.service.IAccountService;

public class AccountService implements IAccountService {

    private IAccountDao accountDao;

    @Override
    public void saveAccount() {
        if (this.accountDao != null) {
            this.accountDao.saveAccount();
        } else {
            System.out.println("Account Dao 为空");
        }
    }
}
