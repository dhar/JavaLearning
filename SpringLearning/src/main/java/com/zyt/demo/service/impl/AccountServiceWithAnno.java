package com.zyt.demo.service.impl;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.jws.WebResult;

/**
 * 1. 对象注解，有以下四个类别，主要是为了区分业务范围
 * Component
 * Service 业务层注解
 * Repository Dao层注解
 * Controller 表现层注解
 *
 * 2. 属性注解
 *  Autowired 根据类型自动查找实现类（如果有多个需要指定Qualifier）
 *  Qualifier 给类属性注入指定bean的id,这种场景下不能单独使用
 *  Resource 直接使用bean的id注入
 *  Value 注入基本数据类型和String类型
 */
@Service("accService_anno")
//@Scope("protoType")
public class AccountServiceWithAnno implements IAccountService {

//    @Autowired
//    @Qualifier("accountDao_anno")
    @Resource(name = "accountDao_anno")
    private IAccountDao accountDao;

    @Value("test value")
    private String name;

    @Override
    public void saveAccount() {
        System.out.println("name = " + name);
        if (this.accountDao != null) {
            this.accountDao.saveAccount();
        } else {
            System.out.println("Account Dao 为空");
        }
    }

    @PostConstruct
    public void init() {
        System.out.println("Service创建");
    }

    @PreDestroy
    public void destory() {
        System.out.println("Service销毁");
    }
}
