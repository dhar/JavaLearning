package com.zyt.demo.factory;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.dao.impl.AccountDao;

public class MyInstanceBeanFactory {
    public IAccountDao getAccountDao() {
        return new AccountDao();
    }
}
