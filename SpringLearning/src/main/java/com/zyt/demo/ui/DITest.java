package com.zyt.demo.ui;

import com.zyt.demo.dao.impl.LifeCircleDemoDao;
import com.zyt.demo.entity.DIDemoEntity;
import com.zyt.demo.entity.DIDemoSetterEntity;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DITest {
    @Test
    public void testDiWithConstructor() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        DIDemoEntity diDemoEntity = context.getBean("diDemoEntity", DIDemoEntity.class);
        System.out.println("diDemoEntity = " + diDemoEntity);
    }

    @Test
    public void testDiWithSetter() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        DIDemoSetterEntity diDemoEntity = context.getBean("diDemoSetterEntity", DIDemoSetterEntity.class);
        System.out.println("diDemoEntity = " + diDemoEntity);
    }
}