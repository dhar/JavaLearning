package com.zyt.demo.ui;

import com.zyt.demo.dao.IAccountDao;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class CustomFactory {
    @Test
    public void testBeanFromCustomFactory() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        IAccountDao accountDao = context.getBean("accountDaoFromFactory", IAccountDao.class);
        accountDao.saveAccount();
    }

    @Test
    public void testBeanFromCustomStaticFactory() {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        IAccountDao accountDao = context.getBean("accountDaoFromStaticFactory", IAccountDao.class);
        accountDao.saveAccount();
    }

}
