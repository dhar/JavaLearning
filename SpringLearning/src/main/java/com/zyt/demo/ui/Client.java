package com.zyt.demo.ui;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.service.IAccountService;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;


public class Client {
    public static void main(String[] args) {

    }

    @Test
    public void testContext() {
        /**
         * ClassPathXmlApplicationContext 加载类路径下的配置文件
         * FileSystemXmlApplicationContext 加载文件系统的配置文件
         * AnnotationConfigApplicationContext 加载注解的的配置文件
         *
         * Context 获取到的是单例对象
         */

        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        {
            IAccountDao accountDao = (IAccountDao) context.getBean("accountDao");
            IAccountService accountService = context.getBean("accountService", IAccountService.class);
            System.out.println("accountService = " + accountService);
            System.out.println("accountDao = " + accountDao);
        }

        {
            IAccountDao accountDao = (IAccountDao) context.getBean("accountDao");
            IAccountService accountService = context.getBean("accountService", IAccountService.class);
            System.out.println("accountService = " + accountService);
            System.out.println("accountDao = " + accountDao);
        }
    }

    @Test
    public void testContext2() {
        /**
         * ClassPathXmlApplicationContext 加载类路径下的配置文件
         * FileSystemXmlApplicationContext 加载文件系统的配置文件
         * AnnotationConfigApplicationContext 加载注解的的配置文件
         *
         * Context 获取到的是单例对象
         */

        {
            ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
            IAccountDao accountDao = (IAccountDao) context.getBean("accountDao");
            System.out.println("accountDao = " + accountDao);
        }

        {
            ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
            IAccountDao accountDao = (IAccountDao) context.getBean("accountDao");
            System.out.println("accountDao = " + accountDao);
        }
    }

    @Test
    public void testContext3() {
        /**
         * ApplicationContext 采用立即加载的策略，在创建Context的时候创建bean对象
         */
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
    }

    @Test
    public void testContext4() {
        /**
         * ApplicationContext 采用立即加载的策略，在创建Context的时候创建bean对象
         */
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        IAccountService accountService = context.getBean("accountService", IAccountService.class);
        accountService.saveAccount();
    }

    @Test
    public void testFactory() {
        /**
         * Factory 获取到的是多例对象
         */
        Resource resource = new ClassPathResource("bean.xml");
        BeanFactory factory = new XmlBeanFactory(resource);
        {
            IAccountDao accountDao = (IAccountDao) factory.getBean("accountDao");
            System.out.println("accountDao = " + accountDao);
        }

        {
            IAccountDao accountDao = (IAccountDao) factory.getBean("accountDao");
            System.out.println("accountDao = " + accountDao);
        }
    }

    @Test
    public void testFactor2y() {
        /**
         * Factory 获取到的是多例对象
         */
        Resource resource = new ClassPathResource("bean.xml");
        {
            BeanFactory factory = new XmlBeanFactory(resource);
            IAccountDao accountDao = (IAccountDao) factory.getBean("accountDao");
            System.out.println("accountDao = " + accountDao);
        }

        {
            BeanFactory factory = new XmlBeanFactory(resource);
            IAccountDao accountDao = (IAccountDao) factory.getBean("accountDao");
            System.out.println("accountDao = " + accountDao);
        }
    }

    @Test
    public void testFactor3() {
        /**
         * Factory使用延迟加载，在需要的时候创建对象
         */
        Resource resource = new ClassPathResource("bean.xml");
        BeanFactory factory = new XmlBeanFactory(resource);
    }
}

