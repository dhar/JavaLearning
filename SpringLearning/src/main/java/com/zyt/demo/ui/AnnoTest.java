package com.zyt.demo.ui;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.dao.impl.AccountDao;
import com.zyt.demo.dao.impl.LifeCircleDemoDao;
import com.zyt.demo.service.IAccountService;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AnnoTest {
    @Test
    public void testAccountAnno() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean_anno.xml");
        IAccountDao accountDao = context.getBean("accountDao_anno", IAccountDao.class);
        System.out.println("accountDao = " + accountDao);
        accountDao.saveAccount();
        context.close();
    }

    @Test
    public void testAccountServiceAnno() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean_anno.xml");
        IAccountService accountService = context.getBean("accService_anno", IAccountService.class);
        accountService.saveAccount();
        context.close();
    }
}
