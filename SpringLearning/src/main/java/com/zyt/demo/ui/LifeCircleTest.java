package com.zyt.demo.ui;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.dao.impl.LifeCircleDemoDao;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class LifeCircleTest {
    @Test
    public void testLiftCircle() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        LifeCircleDemoDao accountDao = context.getBean("lifeCircleDemoDao", LifeCircleDemoDao.class);
        System.out.println("accountDao = " + accountDao);
        context.close();
    }
}
