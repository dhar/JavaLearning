package com.zyt.demo.dao.impl;

import com.zyt.demo.dao.IAccountDao;
import org.springframework.stereotype.Repository;

@Repository("accountDao_anno2")
public class AccountDaoWithAnno2 implements IAccountDao {
    @Override
    public void saveAccount() {
        System.out.println("AccountDaoWithAnno2 save");
    }
}
