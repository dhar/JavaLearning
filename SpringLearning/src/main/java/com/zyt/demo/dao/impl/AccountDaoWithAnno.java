package com.zyt.demo.dao.impl;

import com.zyt.demo.dao.IAccountDao;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component("accountDao_anno")
public class AccountDaoWithAnno implements IAccountDao {
    @Override
    public void saveAccount() {
        System.out.println("AccountDaoWithAnno save");
    }
}
