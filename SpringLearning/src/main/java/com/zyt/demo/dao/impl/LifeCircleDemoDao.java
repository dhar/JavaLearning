package com.zyt.demo.dao.impl;

public class LifeCircleDemoDao {
    public LifeCircleDemoDao() {
    }

    public void init() {
        System.out.println("对象创建了。。。");
    }

    public void destory() {
        System.out.println("对象销毁了。。。");
    }
}
