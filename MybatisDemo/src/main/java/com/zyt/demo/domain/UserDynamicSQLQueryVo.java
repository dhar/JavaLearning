package com.zyt.demo.domain;

import java.io.Serializable;
import java.util.ArrayList;

public class UserDynamicSQLQueryVo implements Serializable {
    private ArrayList ids;

    public ArrayList getIds() {
        return ids;
    }

    public void setIds(ArrayList ids) {
        this.ids = ids;
    }
}
