package com.zyt.demo.domain;

public class Account {
    private Integer money;
    private Integer uid;
    private Integer id;
    private User user;//一对一关系，账户所对应的用户

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Account{" +
                "money=" + money +
                ", uid=" + uid +
                ", id=" + id +
                ", user=" + user +
                '}';
    }
}
