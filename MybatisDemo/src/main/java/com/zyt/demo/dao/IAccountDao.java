package com.zyt.demo.dao;

import com.zyt.demo.domain.Account;
import com.zyt.demo.domain.Role;

import java.util.List;

public interface IAccountDao {
    /**
     * 查询所有账号
     * @return 账号数组
     */
    List<Account> findAll();

    /**
     * 查询所有账号
     * @return 账号数组
     */
    List<Account> findAllWithLazyMode();
}
