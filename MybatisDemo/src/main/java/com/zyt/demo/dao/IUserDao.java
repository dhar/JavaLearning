package com.zyt.demo.dao;

import com.zyt.demo.domain.QueryVO;
import com.zyt.demo.domain.User;

import java.util.List;

public interface IUserDao {
    /**
     * 查询所有用户
     * @return 用户数组
     */
    List<User> findAll();

    /**
     * 根据id查询用户信息
     * @param userId
     * @return
     */
    User findById(Integer userId);

    /**
     * 保存用户
     * @param user 用户
     */
    void saveUser(User user);

    /**
     * 更新已有用户，用户的ID需要存在表中
     * @param user 用户
     */
    void updateUser(User user);

    /**
     * 删除对应ID的用户
     * @param uid 用户ID
     */
    void deleteUserByID(Integer uid);

    /**
     * 模糊查找用户名所对应的用户
     * @param likeName 模糊条件
     * @return 用户数组
     */
    List findUserByLikeName(String likeName);

    /**
     * 使用QueryVO查询
     * @param vo QueryVO对象
     * @return 用户数组
     */
    List findUserByVO(QueryVO vo);
}

