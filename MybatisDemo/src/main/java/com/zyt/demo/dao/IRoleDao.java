package com.zyt.demo.dao;

import com.zyt.demo.domain.Role;
import com.zyt.demo.domain.User;

import java.util.List;

public interface IRoleDao {
    /**
     * 查询所有角色
     * @return 角色数组
     */
    List<Role> findAll();
}
