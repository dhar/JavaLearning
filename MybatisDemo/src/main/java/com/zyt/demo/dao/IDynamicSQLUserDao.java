package com.zyt.demo.dao;

import com.zyt.demo.domain.User;
import com.zyt.demo.domain.UserDynamicSQLQueryVo;

import java.util.List;

public interface IDynamicSQLUserDao {
    /**
     * 查询所有用户
     * @return 用户数组
     */
    List<User> findAll();

    List<User> findByUserIf(User user);

    List<User> findByUserWhere(User user);

    List<User> findByUserForeach(UserDynamicSQLQueryVo queryVo);
}
