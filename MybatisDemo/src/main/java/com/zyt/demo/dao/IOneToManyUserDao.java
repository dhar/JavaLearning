package com.zyt.demo.dao;

import com.zyt.demo.domain.User;

import java.util.List;

public interface IOneToManyUserDao {
    /**
     * 查询所有用户
     * @return 用户数组
     */
    List<User> findAll();
}
