package com.zyt.demo.dao;

import com.zyt.demo.domain.AnnoUser;
import com.zyt.demo.domain.User;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface IUserAnnoDao {
    @Select(value = "SELECT * FROM eesy_mybatis.user")
    @Results(
            id = "UserMap",
            value = {
                    @Result(id = true, column = "username", property = "username"),
                    @Result(column = "username", property = "username"),
                    @Result(column = "birthday", property = "userBirthday"),
                    @Result(column = "address", property = "userAddress"),
                    @Result(column = "sex", property = "userSex"),
            }
    )
    public List<AnnoUser> findAll();
}
