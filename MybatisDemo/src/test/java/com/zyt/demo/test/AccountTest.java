package com.zyt.demo.test;

import com.zyt.demo.dao.IAccountDao;
import com.zyt.demo.dao.IRoleDao;
import com.zyt.demo.domain.Account;
import com.zyt.demo.domain.Role;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class AccountTest {
    private InputStream is;
    private SqlSession session;

    @Before
    public void init() throws IOException {
        this.is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        this.session = factory.openSession();
    }

    @After
    public void destory() throws IOException {
        this.session.close();
        this.is.close();
    }

    @Test
    public void testFindAllWithLazyMod() {
        IAccountDao accountDao = session.getMapper(IAccountDao.class);
        List<Account> accounts = accountDao.findAllWithLazyMode();
        System.out.println("accounts.size = " + accounts.size());
//        System.out.println("accounts = " + accounts);
        for (Account accout: accounts) {
//            System.out.println("accout = " + accout);
            System.out.println("accout.getMoney() = " + accout.getMoney());
            System.out.println("accout.getUser() = " + accout.getUser());
        }
    }
}
