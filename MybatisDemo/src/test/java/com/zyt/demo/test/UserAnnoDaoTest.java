package com.zyt.demo.test;

import com.zyt.demo.dao.IUserAnnoDao;
import com.zyt.demo.dao.IUserDao;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class UserAnnoDaoTest {
    private InputStream is;
    private SqlSession session;

    @Before
    public void init() throws IOException {
        this.is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        this.session = factory.openSession();
    }

    @After
    public void destory() throws IOException {
        this.session.close();
        this.is.close();
    }

    @Test
    public void testFindAll() {
        IUserAnnoDao userDao = session.getMapper(IUserAnnoDao.class);
        List users = userDao.findAll();
        System.out.println("users.size() = " + users.size());
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }

}
