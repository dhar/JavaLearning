package com.zyt.demo.test;

import com.zyt.demo.dao.IDynamicSQLUserDao;
import com.zyt.demo.dao.IUserDao;
import com.zyt.demo.domain.User;
import com.zyt.demo.domain.UserDynamicSQLQueryVo;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DynamicSQLUserDaoTest {
    private InputStream is;
    private SqlSession session;

    @Before
    public void init() throws IOException {
        this.is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        this.session = factory.openSession();
    }

    @After
    public void destory() throws IOException {
        this.session.close();
        this.is.close();
    }

    @Test
    public void testFindAll() {
        IDynamicSQLUserDao userDao = session.getMapper(IDynamicSQLUserDao.class);
        List users = userDao.findAll();
        System.out.println("users.size() = " + users.size());
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }

    @Test
    public void testFindByUserIf() {
        IDynamicSQLUserDao userDao = session.getMapper(IDynamicSQLUserDao.class);
        User pUser = new User();
        pUser.setUsername("%王%");
        List users = userDao.findByUserIf(pUser);
        System.out.println("users.size() = " + users.size());
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }

    @Test
    public void testFindByUserWhere() {
        IDynamicSQLUserDao userDao = session.getMapper(IDynamicSQLUserDao.class);
        User pUser = new User();
        pUser.setUsername("%王%");
        List users = userDao.findByUserWhere(pUser);
        System.out.println("users.size() = " + users.size());
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }

    @Test
    public void testFindByUserForeach() {
        IDynamicSQLUserDao userDao = session.getMapper(IDynamicSQLUserDao.class);
        UserDynamicSQLQueryVo queryVo = new UserDynamicSQLQueryVo();
        ArrayList ids = new ArrayList();
        ids.add(41);
        ids.add(42);
        queryVo.setIds(ids);
        List users = userDao.findByUserForeach(queryVo);
        System.out.println("users.size() = " + users.size());
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }
}
