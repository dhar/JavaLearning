package com.zyt.demo.test;

import com.zyt.demo.dao.IRoleDao;
import com.zyt.demo.dao.IUserDao;
import com.zyt.demo.domain.Role;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class RoleTest {
    private InputStream is;
    private SqlSession session;

    @Before
    public void init() throws IOException {
        this.is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        this.session = factory.openSession();
    }

    @After
    public void destory() throws IOException {
        this.session.close();
        this.is.close();
    }

    @Test
    public void testFindAll() {
        IRoleDao roleDao = session.getMapper(IRoleDao.class);
        List<Role> roles = roleDao.findAll();
        System.out.println("roles.size = " + roles.size());
        for (int i = 0; i < roles.size(); i++) {
            Role rol = roles.get(i);
            System.out.println("rol = " + rol);
        }
    }
}
