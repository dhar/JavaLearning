package com.zyt.demo.test;

import com.zyt.demo.dao.IUserDao;
import com.zyt.demo.domain.QueryVO;
import com.zyt.demo.domain.User;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.management.Query;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

public class UserTest {
    public static void main(String[] args) throws IOException {
        InputStream is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        SqlSession session = factory.openSession();

        IUserDao userDao = session.getMapper(IUserDao.class);
        List users = userDao.findAll();
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }

        session.close();
        is.close();
    }

    private InputStream is;
    private SqlSession session;

    @Before
    public void init() throws IOException {
        this.is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        this.session = factory.openSession();
    }

    @After
    public void destory() throws IOException {
        this.session.close();
        this.is.close();
    }

    @Test
    public void testSession() throws IOException {
        InputStream is = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory factory = builder.build(is);
        SqlSession session = factory.openSession();

        IUserDao userDao = session.getMapper(IUserDao.class);
        List users = userDao.findAll();
        System.out.println("users = " + users);

        session.close();
        is.close();
    }

    @Test
    public void testFindAll() {
        IUserDao userDao = session.getMapper(IUserDao.class);
        List users = userDao.findAll();
        System.out.println("users.size() = " + users.size());
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }

    @Test
    public void testSave() {
        User user = new User();
        user.setUsername("张玉堂");
        user.setAddress("福建福州");
        user.setSex("男");
        user.setBirthday(new Date());
        System.out.println("before insert user = " + user);

        IUserDao userDao = this.session.getMapper(IUserDao.class);
        userDao.saveUser(user);
        this.session.commit();
        System.out.println("after insert user = " + user);
    }

    @Test
    public void testUpdate() {
        User user = new User();
        user.setId(50);
        user.setUsername("张玉堂-UpdateByMybatics");
        user.setAddress("福建福州");
        user.setSex("男");
        user.setBirthday(new Date());

        IUserDao userDao = this.session.getMapper(IUserDao.class);
        userDao.updateUser(user);
        this.session.commit();
        System.out.println("user = " + user);
    }

    @Test
    public void testDeleteUserByID() {
        IUserDao userDao = this.session.getMapper(IUserDao.class);
        userDao.deleteUserByID(52);
        this.session.commit();
    }

    @Test
    public void testFindUserByLikeName() {
        IUserDao userDao = this.session.getMapper(IUserDao.class);
        List users = userDao.findUserByLikeName("%王%");
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }

    @Test
    public void testFindUserbyID() {
        IUserDao userDao = this.session.getMapper(IUserDao.class);
        User user1 = userDao.findById(41);
        System.out.println("user1 = " + user1.hashCode());

        User user2 = userDao.findById(41);
        System.out.println("user2 = " + user2.hashCode());

        // clear Cache 之后会重新查询数据库
        this.session.clearCache();

        User user3 = userDao.findById(41);
        System.out.println("user3 = " + user3.hashCode());
    }

    @Test
    public void testClearCache() {
        IUserDao userDao = this.session.getMapper(IUserDao.class);
        User user2 = userDao.findById(42);
        System.out.println("user2 = " + user2.hashCode());

        user2.setUsername("hh_" + user2.getUsername());
        userDao.saveUser(user2);

        // 如果数据库发生增删改查操作，mybatis会清空一级缓存
        User user3 = userDao.findById(42);
        System.out.println("user3 = " + user3.hashCode());
    }

    @Test
    public void testFindUserByQueryVO() {
        IUserDao userDao = this.session.getMapper(IUserDao.class);
        QueryVO vo = new QueryVO();
        User qUser = new User();
        qUser.setUsername("%王%");
        vo.setUser(qUser);
        List users = userDao.findUserByVO(vo);
        for (int i = 0; i < users.size(); i++) {
            System.out.println("users.get(i) = " + users.get(i));
        }
    }
}


